# Google Translation API Implementation

### This project is implemented in Java and Javascript technologies. The project is build using gradle and use google cloud API to perform translations.

## Key Steps to Set up:

1.  Install Google Cloud CLI in the machine
2.  Login into the gcloud and google cloud console. Needed to add Billing details.
3.  create a project id (say, translationstorageapianuja)
4. Clone the skeleton project repo provided in the challenge.
5. Create App Engine in the project.
6. Enable the translation API.
7. Create service account key
8. Create two Buckets in the west Europe region. (one for storing original files and other for storing translations)
9. Solution can be tested using the below URL:
 https://translationstorageapianuja.nw.r.appspot.com/