package com.qlouder;

import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

public class Constants {
    public static final int FILE_ALREADY_TRANSLATED = 100;
    public static final int FILE_ADDED = 101;
}
