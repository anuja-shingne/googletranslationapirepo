package com.qlouder;

public class TranslationSuccessResponse {
    private String translatedFilename;
    private String sourceLang;
    private String destinationLang;
    private String message;
    private int code;

    public String getTranslatedFilename() {
        return translatedFilename;
    }

    public void setTranslatedFilename(String translatedFilename) {
        this.translatedFilename = translatedFilename;
    }

    public String getSourceLang() {
        return sourceLang;
    }

    public void setSourceLang(String sourceLang) {
        this.sourceLang = sourceLang;
    }

    public String getDestinationLang() {
        return destinationLang;
    }

    public void setDestinationLang(String destinationLang) {
        this.destinationLang = destinationLang;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}