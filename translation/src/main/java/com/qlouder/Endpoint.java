package com.qlouder;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.config.Nullable;
import com.google.api.server.spi.response.BadRequestException;
import com.google.api.server.spi.response.NotFoundException;
import com.google.cloud.storage.*;
import com.google.cloud.translate.Translate;
import com.google.cloud.translate.v3.LocationName;
import com.google.cloud.translate.v3.TranslateTextRequest;
import com.google.cloud.translate.v3.TranslateTextResponse;
import com.google.cloud.translate.v3.TranslationServiceClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Api(name = "api", version = "v1")
public class Endpoint {
    private static final String bucketName = "anujatranslationapibucket";
    private static final String originalFiles = "originalfilesapianuja";
    private static final Storage storage = StorageOptions.getDefaultInstance().getService();
    private static final Bucket bucket = storage.get(bucketName);
    private static final Bucket originalFilesBucket = storage.get(originalFiles);

    @ApiMethod(name = "translate", httpMethod = "GET")
    public TranslationSuccessResponse translate(@Named("file") @Nullable String fileName, @Named("lang") @Nullable String lang) throws NotFoundException, BadRequestException, IOException {
        if (fileName == null || fileName.isEmpty()) {
            throw new BadRequestException("parameter file can not be empty");
        }

//        Page<Blob> blobs = bucket.list(Storage.BlobListOption.prefix("baseFiles/"));
//        Blob targetBlob = null;
//        Blob blob = null;
//        Iterator<Blob> baseFilesIterator = blobs.getValues().iterator();
//        if (baseFilesIterator.hasNext()) {
//            Blob originalFile = baseFilesIterator.next();
//            if (originalFile.getName().equalsIgnoreCase(fileName))
//                blob = originalFile;
//        }

        Blob blob = storage.get(BlobId.of(originalFiles, fileName));
        if (blob == null) {
            throw new NotFoundException("file name " + fileName + " not found in bucket");
        }
        String inputText = new String(blob.getContent());
        if (inputText.isEmpty()) {
            throw new NotFoundException(fileName + " doesn't contain any text");
        }
        if (lang == null || lang.isEmpty()) {
            lang = "en";
        }
        String targetFileName = fileName.substring(0, fileName.lastIndexOf(".")) + "-" + lang + ".txt";

        Blob targetBlob = storage.get(BlobId.of(bucketName, targetFileName));

        if (targetBlob != null && targetBlob.exists()) {
            TranslationSuccessResponse successResponse = new TranslationSuccessResponse();
            successResponse.setDestinationLang(lang);
            successResponse.setTranslatedFilename(targetFileName);
            successResponse.setMessage("File has already been translated");
            successResponse.setCode(Constants.FILE_ALREADY_TRANSLATED);
            return successResponse;
        }

        Translate.TranslateOption targetLang = Translate.TranslateOption.targetLanguage(lang);

        if (targetLang == null) {
            throw new NotFoundException("Target lang " + lang + " Not found");
        }
        /*Translation translation = translate.translate(inputText,targetLang );

        if(translation==null){
            throw new NotFoundException(fileName+" contents can not be translated");
        }
        String translatedText=translation.getTranslatedText();
        */

        BlobId translatedBlobId = BlobId.of(bucketName, targetFileName);
        BlobInfo translatedBlobInfo = BlobInfo.newBuilder(translatedBlobId).build();

        String translatedText = translateText("translationstorageapianuja", lang, inputText);


        storage.create(translatedBlobInfo, translatedText.getBytes());

        TranslationSuccessResponse successResponse = new TranslationSuccessResponse();
        successResponse.setTranslatedFilename(targetFileName);
        //successResponse.setSourceLang(translation.getSourceLanguage());
        successResponse.setMessage(targetFileName + " file is stored in the cloud successfully.");
        successResponse.setDestinationLang(lang);
        successResponse.setCode(Constants.FILE_ADDED);
        return successResponse;
    }

    @ApiMethod(name = "files", httpMethod = "GET")
    public List<String> files() {
        ArrayList<String> fileDetails = new ArrayList<>();
        //   storage.list(bucketName, Storage.BlobListOption.prefix("baseFiles")).getValues().forEach(item -> {
        storage.list(originalFiles).getValues().forEach(item -> {
            fileDetails.add(item.getName());
        });
        return fileDetails;
    }

    @ApiMethod(name = "translatedFiles", httpMethod = "GET")
    public List<String> translatedFiles() {

        ArrayList<String> fileDetails = new ArrayList<>();
        //  storage.list(bucketName, Storage.BlobListOption.currentDirectory()).getValues().forEach(item -> {
        storage.list(bucketName).getValues().forEach(item -> {
            fileDetails.add(item.getName());
        });
        return fileDetails;
    }


    public static String translateText(String projectId, String targetLanguage, String text)
            throws IOException {

        try (TranslationServiceClient client = TranslationServiceClient.create()) {
            LocationName parent = LocationName.of(projectId, "global");

            TranslateTextRequest request =
                    TranslateTextRequest.newBuilder()
                            .setParent(parent.toString())
                            .setMimeType("text/plain")
                            .setTargetLanguageCode(targetLanguage)
                            .addContents(text)
                            .build();

            TranslateTextResponse response = client.translateText(request);
            StringBuffer sb = new StringBuffer();
            for (com.google.cloud.translate.v3.Translation translation : response.getTranslationsList()) {
                sb.append(translation.getTranslatedText());
            }
            return sb.toString();
        }
    }
}
